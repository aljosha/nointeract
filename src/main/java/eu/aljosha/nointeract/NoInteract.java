package eu.aljosha.nointeract;

import eu.aljosha.nointeract.commands.SetNoInteractCommand;
import eu.aljosha.nointeract.listener.InteractListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class NoInteract extends JavaPlugin {

    @Override
    public void onEnable() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new InteractListener(), this);
        getCommand("nointeract").setExecutor(new SetNoInteractCommand());
    }
}
