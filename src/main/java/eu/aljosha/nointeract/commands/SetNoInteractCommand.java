package eu.aljosha.nointeract.commands;


import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SetNoInteractCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if(!(commandSender instanceof Player)){
            commandSender.sendMessage("This command is only available for players");
            return true;
        }

        Player player = (Player)commandSender;

        if(!player.hasPermission("nointeract.admin")){
            player.sendMessage("§3[§eNoInteract§3] §cSorry, you don´t have permission to execute this command.");
            return true;
        }

        ItemStack itemInMainHand = player.getInventory().getItemInMainHand();

        if(itemInMainHand.getType() == Material.AIR){
            player.sendMessage("§3[§eNoInteract§3] §cSorry, you have to hold an item in your main hand to do this.");
            return true;
        }

        NBTItem nbti = new NBTItem(itemInMainHand);
        nbti.setBoolean("nointeract", true);
        ItemStack modifiedItem = nbti.getItem();
        player.getInventory().setItemInMainHand(modifiedItem);

        player.sendMessage("§3[§eNoInteract§3] §eSuccess! Now nobody can interact with this item.");

        return true;
    }
}
