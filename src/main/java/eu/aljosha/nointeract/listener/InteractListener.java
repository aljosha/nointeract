package eu.aljosha.nointeract.listener;


import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        if (item != null && item.getType() != Material.AIR) {
            NBTItem nbti = new NBTItem(item);
            Boolean nointeract = nbti.getBoolean("nointeract");
            if (nointeract) {
                event.setCancelled(true);
            }
        }
    }


}